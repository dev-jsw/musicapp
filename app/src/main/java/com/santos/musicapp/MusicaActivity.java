package com.santos.musicapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.Image;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

public class MusicaActivity extends AppCompatActivity implements SensorEventListener {
    private long lastUpdate;
    List list;
    MediaPlayer mp;
    Button playBtn;
    Context context;
    ImageView imagenMusica;
    SharedPreferences pref;
    SeekBar positionBar,volumeBar;
    TextView text_cancion, elapsedTimeLabel,remainingTimeLabel, Ejes;

    String idMusic;
    float volumeNum;
    int nivel_volumen, totalTime;
    Boolean activa_acelerometro = true, repetir_cancion= true;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);
        context = MusicaActivity.this;
        pref = PreferenceManager.getDefaultSharedPreferences(this);

        text_cancion = findViewById(R.id.text_cancion);
        Ejes = findViewById(R.id.Ejes);
        positionBar = findViewById(R.id.positionBar);
        volumeBar = findViewById(R.id.volumeBar);
        playBtn = findViewById(R.id.playBtn);
        imagenMusica = findViewById(R.id.imagenMusica);
        elapsedTimeLabel = findViewById(R.id.elapsedTimeLabel);
        remainingTimeLabel = findViewById(R.id.remainingTimeLabel);

        idMusic =  getIntent().getStringExtra("idMusic");
        activa_acelerometro = pref.getBoolean("activa_acelerometro",true);
        repetir_cancion = pref.getBoolean("repetir_cancion",true);
        nivel_volumen = Integer.parseInt(pref.getString("nivel_volumen","50"));

        //ApagarAudio();

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        assert sensorManager != null;
        list = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);

        if(list.size()>0){
            sensorManager.registerListener(this, (Sensor) list.get(0),SensorManager.SENSOR_DELAY_NORMAL);
            Toast.makeText(getBaseContext(), "Bienvenido a Music App", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(getBaseContext(), "Error: No cuenta con acelerómetro", Toast.LENGTH_LONG).show();
        }

        // Media Player
        escogeMusica();

        mp.setLooping(repetir_cancion);
        mp.seekTo(0);
        volumeNum = nivel_volumen/ 100f;
        mp.setVolume(volumeNum, volumeNum);
        volumeBar.setProgress(nivel_volumen);
        totalTime = mp.getDuration();

        // Position Bar
        positionBar.setMax(totalTime);
        positionBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            mp.seekTo(progress);
                            positionBar.setProgress(progress);
                        }
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                }
        );

        // Volume Bar
        volumeBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        volumeNum = progress / 100f;
                        mp.setVolume(volumeNum, volumeNum);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                }
        );

        //Hilo
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (mp != null) {
                    try {
                        Message msg = new Message();
                        msg.what = mp.getCurrentPosition();
                        handler.sendMessage(msg);
                        if (mp!=null && !mp.isPlaying()) { playBtn.setBackgroundResource(R.drawable.play);}
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Log.e("ErrorRunnable",e.toString());
                    }
                }
            }
        }).start();
    }

    public void escogeMusica(){
        if(idMusic.equals("0")){
            mp = MediaPlayer.create(context, R.raw.wakemeup);
            text_cancion.setText("Wake me up");
            imagenMusica.setImageDrawable(getResources().getDrawable(R.drawable.image));
        }
        else if (idMusic.equals("1")){
            mp = MediaPlayer.create(context, R.raw.alone);
            text_cancion.setText("Alone");
            imagenMusica.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher_music));
        }
        else if (idMusic.equals("2")){
            mp = MediaPlayer.create(context, R.raw.dragonforce);
            text_cancion.setText("Dragon Force");
            imagenMusica.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher_music));
        }
        else if(idMusic.equals("3")){
            mp = MediaPlayer.create(context, R.raw.fiestapagana);
            text_cancion.setText("Fiesta Pagana");
            imagenMusica.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher_music));
        }
        else if (idMusic.equals("4")){
            mp = MediaPlayer.create(context, R.raw.paris);
            text_cancion.setText("Paris");
            imagenMusica.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher_music));
        }
        else if (idMusic.equals("5")){
            mp = MediaPlayer.create(context, R.raw.shewolf);
            text_cancion.setText("Shw Wolf");
            imagenMusica.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher_music));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_music, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id. acercaDe) {
            lanzarAcercaDe(null);
            return true;
        }
        if  (id == R.id.action_settings) {
            lanzarPreferencias(null);
            return true;
        }
        if  (id == R.id.cerrar_sesion) {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(R.string.app_name)
                    .setMessage("¿Desea cerrar sesión?")
                    .setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ApagarAudio();
                            Intent intent = new Intent(context, LoginActivity.class);
                            startActivity(intent);
                        }
                    }).setNegativeButton("No", null).show().setCancelable(false);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void lanzarAcercaDe(View view) {
        Intent i = new Intent(this,AcercaDeActivity.class);
        startActivity(i);
    }

    public void lanzarPreferencias(View view) {
        Intent i = new Intent(this,PreferenciasActivity.class);
        startActivity(i);
    }

    public void mostrarPreferencias(){
        String s = "activa_acelerometro: " + activa_acelerometro + ", nivel_volumen: " + nivel_volumen + ", repetir_cancion: " + repetir_cancion;
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void ApagarAudio(){
        if(mp!=null){
            mp.stop();
            mp.release();
            mp = null;
        }
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @SuppressLint("SetTextI18n")
        @Override
        public void handleMessage(Message msg) {
            int currentPosition = msg.what;
            positionBar.setProgress(currentPosition);
            String elapsedTime = createTimeLabel(currentPosition);
            elapsedTimeLabel.setText(elapsedTime);
            String remainingTime = createTimeLabel(totalTime-currentPosition);
            remainingTimeLabel.setText("- " + remainingTime);
        }
    };


    public String createTimeLabel(int time) {
        String timeLabel;
        int min = time / 1000 / 60;
        int sec = time / 1000 % 60;

        timeLabel = min + ":";
        if (sec < 10) timeLabel += "0";
        timeLabel += sec;

        return timeLabel;
    }

    public void playBtnClick(View view) {
        if (!mp.isPlaying()) {
            mp.start();
            playBtn.setBackgroundResource(R.drawable.stop);

        } else {
            mp.pause();
            playBtn.setBackgroundResource(R.drawable.play);
        }

    }

    public  void btnBeforeClick(View view){
        if (mp.isPlaying()) {
            mp.pause();
            playBtn.setBackgroundResource(R.drawable.play);
        }
        if(idMusic.equals("0")){
            idMusic = "5";
        }
        else{
            idMusic = Integer.toString(Integer.parseInt(idMusic)-1);
        }
        escogeMusica();
    }

    public  void btnNextClick(View view){
        if (mp.isPlaying()) {
            mp.pause();
            playBtn.setBackgroundResource(R.drawable.play);
        }
        if(idMusic.equals("5")){
            idMusic = "0";
        }
        else{
            idMusic = Integer.toString(Integer.parseInt(idMusic)+1);
        }
        escogeMusica();
    }
    @Override
    public void onBackPressed() {
        ApagarAudio();
        Intent intent = new Intent(context, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ApagarAudio();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER && pref.getBoolean("activa_acelerometro",true)) {
            float[] values = event.values;
            float x = values[0];
            float y = values[1];
            float z = values[2];
            float accelationSquareRoot = (x * x + y * y + z * z)/(SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
            Ejes.setText("x: "+values[0]+"\ny:"+values[1]+"\nz: "+values[2]);
            long actualTime = System.currentTimeMillis();
            if (accelationSquareRoot >= 1.1) //it will be executed if you shuffle
            {
                if (actualTime - lastUpdate < 150) {
                    return;
                }
                lastUpdate = actualTime;//updating lastUpdate for next shuffle
                if(x>5 && z<4){
                    volumeNum = (volumeBar.getProgress()-10) / 100f;
                    if(volumeNum>-0.1){
                        mp.setVolume(volumeNum, volumeNum);
                        volumeBar.setProgress((int)(volumeNum*100));
                    }
                    else{
                        Toast.makeText(getBaseContext(), "Volumen al mínimo", Toast.LENGTH_LONG).show();
                    }
                }
                else if(x<-5 && z<4){
                    volumeNum = (volumeBar.getProgress()+10) / 100f;
                    if(volumeNum<1.1){
                        mp.setVolume(volumeNum, volumeNum);
                        volumeBar.setProgress((int)(volumeNum*100));
                    }
                    else{
                        Toast.makeText(getBaseContext(), "Volumen al máximo", Toast.LENGTH_LONG).show();
                    }

                }
                else if(-2<x && x<2 && y<3 && z>-9){
                    if(mp!= null && mp.isPlaying()) {
                        mp.pause();
                        playBtn.setBackgroundResource(R.drawable.play);
                    }
                }
                else if(-2<x && x<2 && y>6 && z>-6){
                    if(mp!= null && !mp.isPlaying()){
                        mp.start();
                        playBtn.setBackgroundResource(R.drawable.stop);
                    }
                }
            }
        }
    }
}
