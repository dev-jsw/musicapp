package com.santos.musicapp;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {
    String usuario;
    TextView txt_user;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usuario =  getIntent().getStringExtra("usuario");
        txt_user = findViewById(R.id.text_user);
        txt_user.setText(usuario);

        MyMusicData[] myMusicData = new MyMusicData[]{
                new MyMusicData("Wake me Up","-----"),
                new MyMusicData("Alone","-----"),
                new MyMusicData("Dragon Force","-----"),
                new MyMusicData("Fiesta Pagana","-----"),
                new MyMusicData("Paris","-----"),
                new MyMusicData("Shw Wolf","-----"),
        };

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        MyMusicAdapter adapter = new MyMusicAdapter(myMusicData);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
