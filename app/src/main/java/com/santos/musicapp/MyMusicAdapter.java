package com.santos.musicapp;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyMusicAdapter extends RecyclerView.Adapter<MyMusicAdapter.ViewHolder> {

    private MyMusicData[] musicData;

    // RecyclerView recyclerView;
    public MyMusicAdapter(MyMusicData[] musicData) {
        this.musicData = musicData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_music, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final MyMusicData myMusicData = musicData[position];
        holder.txt_idMusic.setText(String.valueOf(position));
        holder.txt_nombre.setText(musicData[position].getNombreCancion());
        holder.imageMusic.setImageResource(musicData[position].getImgId());
        holder.linearLayout.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(v.getContext(), MusicaActivity.class);
                        intent.putExtra("idMusic",String.valueOf(position));
                        v.getContext().startActivity(intent);
                    }
                });
        holder.linearLayout.setOnLongClickListener(
                new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        Toast.makeText(v.getContext(), "Canción: " + myMusicData.getNombreCancion(), Toast.LENGTH_SHORT).show();
                        return true;
                    }
                }
        );
    }

    @Override
    public int getItemCount() {
        return musicData.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_idMusic, txt_nombre;
        public ImageView imageMusic;
        public LinearLayout linearLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.txt_idMusic = itemView.findViewById(R.id.txt_idMusic);
            this.txt_nombre = itemView.findViewById(R.id.txt_nombre);
            this.imageMusic = itemView.findViewById(R.id.imageMusic);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.linearLayout);
        }
    }
}
