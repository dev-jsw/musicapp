package com.santos.musicapp;

public class MyMusicData {
    private int idMusic;
    private String nombreCancion;
    private String descripcion;
    private int imgId;

    public MyMusicData (String nombreCancion,String descripcion, int imgId) {
        this.nombreCancion = nombreCancion;
        this.descripcion = descripcion;
        this.imgId = imgId;
    }

    public MyMusicData (String nombreCancion,String descripcion) {
        this.nombreCancion = nombreCancion;
        this.descripcion = descripcion;
    }

    public int getIdMusic() {
        return idMusic;
    }

    public String getNombreCancion() {
        return nombreCancion;
    }

    public void setNombreCancion(String nombreCancion) {
        this.nombreCancion = nombreCancion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }
}
