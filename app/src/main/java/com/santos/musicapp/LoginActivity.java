package com.santos.musicapp;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


public class LoginActivity extends AppCompatActivity  {

    TextView txt_codusu, txt_passwd;
    Button btn_logins;
    Context context;
    CheckBox cbx_save;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = LoginActivity.this;
        txt_codusu = findViewById(R.id.txt_codusu);
        txt_passwd = findViewById(R.id.txt_passwd);
        btn_logins = findViewById(R.id.btn_logins);
        cbx_save = findViewById(R.id.cbx_save);

        pref = getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        if(pref.contains("key_cbx") && pref.contains("key_user")){
            cbx_save.setChecked(pref.getBoolean("key_cbx",true));
            txt_codusu.setText(pref.getString("key_user", ""));
            txt_passwd.requestFocus();
        }

        cbx_save.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor = pref.edit();
                if(isChecked && !txt_codusu.getText().toString().equals("")){
                    editor.putBoolean("key_cbx", true);
                    editor.putString("key_user",txt_codusu.getText().toString());
                    editor.commit();
                }
                else{
                    editor.remove("key_cbx");
                    editor.remove("key_user");
                    editor.commit();
                }
            }
        });

        btn_logins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txt_codusu.getText().toString().trim().equals("Jsantosw") && txt_passwd.getText().toString().equals("456")){
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.putExtra("usuario",txt_codusu.getText().toString());
                    startActivity(intent);
                    finish();
                }
                else{
                    Toast.makeText(context, "Usuario/Contraseña incorrecta", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_lock_power_off)
                .setTitle(R.string.app_name).setMessage("¿Desea salir de la aplicación?")
                .setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent a = new Intent(Intent.ACTION_MAIN);
                a.addCategory(Intent.CATEGORY_HOME);
                a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        }).setNegativeButton("No", null).show().setCancelable(false);
    }
}