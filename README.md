# MusicApp!
Hola! Esta es una aplicación para escuchar **música** fue desarrollada 
para la plataforma **Android API 21** en adelante. Cuenta con lectura de sensor 
acelerómetro para comenzar o parar la canción. Para la selección de las canciones 
se debe mantener presionado el item.

# Inicio de Sesión

Para iniciar sesión correctamente el usuario es **Jsantosw** y la contraseña es **456**

## Canciones

Actualmente la aplicación cuenta con 6 canciones escogidas al azar
 - Wake me up
 - Alone
 - Dragon Force
 - Fiesta
 - Paris
 - Shw wolf

	> **Note:** La aplicación aún está en desarrollo para que se puedan leer canciones del mismo móvil.